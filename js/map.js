function initialize() {
    var mapCanvas = document.getElementById('map');
    var mapOptions = {
        center: new google.maps.LatLng(34.15157, -118.11281),
        zoom: 40,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var myLatLang = (34.15157, -118.11281);
}

google.maps.event.addDomListener(window, 'load', initialize);
var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: iconBase + 'logo.jpg'
});